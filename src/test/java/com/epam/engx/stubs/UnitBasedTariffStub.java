package com.epam.engx.stubs;

import com.epam.engx.thirdpartyjar.Tariff;
import com.epam.engx.thirdpartyjar.TariffType;

public class UnitBasedTariffStub implements Tariff {

    private double additionalFee;

    public UnitBasedTariffStub() {
        additionalFee = 0;
    }


    public UnitBasedTariffStub(double additionalFee) {
        this.additionalFee = additionalFee;
    }

    @Override
    public double getAdditionalFee() {
        return additionalFee;
    }

    private static class UnitBasedTariffTypeStub implements TariffType {
        @Override
        public boolean isUnitBased() {
            return true;
        }

    }

    @Override
    public TariffType getType() {
        return new UnitBasedTariffTypeStub();
    }
}
