package com.epam.engx.stubs;

import com.epam.engx.thirdpartyjar.Tariff;
import com.epam.engx.thirdpartyjar.TariffType;

public class NoRateTariffStub implements Tariff {

    private double additionalFee;

    public NoRateTariffStub() {
        additionalFee = 0;
    }

    public NoRateTariffStub(double additionalFee) {
        this.additionalFee = additionalFee;
    }

    @Override
    public double getAdditionalFee() {
        return additionalFee;
    }

    private static class NoRateTariffTypeStub implements TariffType {
        @Override
        public boolean isUnitBased() {
            return false;
        }
    }

    @Override
    public TariffType getType() {
        return new NoRateTariffTypeStub();
    }
}